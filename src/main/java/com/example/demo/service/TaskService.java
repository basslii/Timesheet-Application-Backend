package com.example.demo.service;

import com.example.demo.classes.Task;
import org.springframework.data.domain.Page;

import java.util.Map;
import java.util.List;

public interface TaskService {
    Task createTask(Task task);
    Page<Task> getAllTasksByKeyword(String keyword, int page);
    Task updateTask(Task task);
    void deleteTask(Integer id);

    Integer getTotalTasks();
}
