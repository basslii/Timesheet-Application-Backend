package com.example.demo.repositories;

import com.example.demo.classes.Task;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.domain.Pageable;

public interface TaskRepository extends JpaRepository<Task, Integer> {
    Page<Task> findByTaskContaining(String keyword, Pageable pageable);
}
